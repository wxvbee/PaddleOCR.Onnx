﻿// Copyright (c) 2021 raoyutian Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
using System.Drawing;
namespace PaddleOCR.Onnx
{
   /// <summary>
   /// 识别的文本块
   /// </summary>
    public sealed class TextBlock
    {
        public List<Point> BoxPoints { get; set; }
        public float BoxScore { get; set; }
        public int AngleIndex { get; set; }
        public float AngleScore { get; set; }
        public float AngleTime { get; set; }
        public string Text { get; set; }
        public List<float> CharScores { get; set; }
        public float CrnnTime { get; set; }
        public float BlockTime { get; set; }
      
    } 
}

