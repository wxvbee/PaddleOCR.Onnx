﻿using PaddleOCR.Onnx;

using System;
using System.IO;
using System.Linq;
namespace OnnxConsoleDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] extensions = new string[] { ".jpg", ".bmp", ".jpeg", ".png", ".tif", ".tiff" };
            string imageroot = Environment.CurrentDirectory + "\\image";
            if (!Directory.Exists(imageroot)) Directory.CreateDirectory(imageroot);
            DirectoryInfo directoryInfo = new DirectoryInfo(imageroot);
            var files = directoryInfo.GetFiles("*.*");

            OCRParameter oCRParameter = new OCRParameter();
            oCRParameter.MaxSideLen = 960;
            oCRParameter.numThread = 6;
            oCRParameter.MostAngle = true;
            oCRParameter.DoAngle = true;
            oCRParameter.Enable_mkldnn = true;
            oCRParameter.use_gpu = false;
            oCRParameter.use_angle_cls = false;
            oCRParameter.use_custom_model = false;
            oCRParameter.visualize = true;
            OCRModelConfig config = null;
            PaddleOCREngine paddleOCREngine = new PaddleOCREngine(config, oCRParameter);
            int count = 1;
            DateTime dt1 = DateTime.Now;
            DateTime dt2 = DateTime.Now;
            OCRResult result = null;

            for (int i = 0; i < 1; i++)
            {
                foreach (var file in files)
                {
                    Console.WriteLine($"第{count}个文件");
                    {
                        if (!extensions.Contains(file.Extension.ToLower())) continue;
                      
                        dt1 = DateTime.Now;
                        result = paddleOCREngine.DetectText(file.FullName);

                       // result = paddleOCREngine.DetectText(File.ReadAllBytes(   file.FullName));
                        dt2 = DateTime.Now;

                        Console.WriteLine(result.Text + "\n");

                        Console.WriteLine((dt2- dt1).TotalMilliseconds+"ms\n");
                       
                        GC.Collect();
                        count++;
                    }
                }
            }
            Console.ReadLine();
        }

    }
}
